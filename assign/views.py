from django.shortcuts import render
from .forms import JourneyForm
from .models import Journey
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.db.models import Sum
from django.db.models import F


class Passenger(CreateView):
    template_name = 'passenger.html'
    form_class = JourneyForm
    success_url = '/thanks/'

class TotalKMVehicleList(ListView):
    template_name = 'total_km_vehicle.html'
    model = Journey
    context_object_name = 'passengers'

    def get_queryset(self):
        qs = super().get_queryset()
        _from = self.request.GET.get('from')
        _to = self.request.GET.get('to')
        if _from and _to:
            qs = qs.filter(start_at__gte=_from, start_at__lte=_to)
        return qs.values('vehicle_reg_num').annotate(score = Sum('kilometers'))


class TotalKMUserVehicleList(ListView):
    template_name = 'total_km_user_vehicle.html'
    context_object_name = 'passengers'
    queryset = Journey.objects.values('vehicle_reg_num', username=F('passengers__username')).annotate(score = Sum('kilometers'))