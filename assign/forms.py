from django.forms import ModelForm
from .models import Journey

class JourneyForm(ModelForm):
     class Meta:
        model = Journey
        fields = ['vehicle_reg_num', 'kilometers', 'start_at', 'passengers']
        