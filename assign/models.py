from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal

# Create your models here.

class Journey(models.Model):
    vehicle_reg_num = models.CharField(max_length=30)
    kilometers = models.DecimalField(max_digits=6, decimal_places=1)
    start_at = models.DateTimeField()
    passengers = models.ManyToManyField(User)